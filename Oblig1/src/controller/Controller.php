<?php
include_once("model/DBModel.php");
include_once("model/Book.php");
include_once("view/BookListView.php");
include_once("view/BookView.php");
include_once("view/ErrorView.php");

/** The Controller is responsible for handling user requests, for exchanging data with the Model,
 * and for passing user response data to the various Views.
 * @author Rune Hjelsvold
 * @see model/Model.php The Model class holding book data.
 * @see view/viewbook.php The View class displaying information about one book.
 * @see view/booklist.php The View class displaying information about all books.
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Controller {
	public $model;

	public const OP_PARAM_NAME = 'op';
	public const DEL_OP_NAME = 'del';
	public const ADD_OP_NAME = 'add';
	public const MOD_OP_NAME = 'mod';

	public function __construct()
   {
		session_start();
		$this->model = new DBModel();
		if (!$this->model->conn) // No user connected to DB
		{
			$view = new ErrorView("No DB connection");
			$view->create();
			die();
		}
    }

/** The one function running the controller code.
 */
	public function invoke()
	{
		if (isset($_GET['id']))
		{
			// show the requested book
			$book = $this->model->getBookById($_GET['id']);
			if ($book)
			{
				$view = new BookView($book);
				$view->create();
			}
			else
			{
				$view = new ErrorView("Book " . $_GET['id'] . " not found");
				$view->create();
			}
		}
		else
		{
			if (isset($_POST[self::OP_PARAM_NAME]))//A book record is to be added, deleted, or modified
			{
				switch($_POST[self::OP_PARAM_NAME])
				{
				case self::ADD_OP_NAME :
				    $book = new Book($_POST['title'], $_POST['author'], $_POST['description']);
				    if (!$this->model->addBook($book))
						{
							$view = new ErrorView("Failed to add " . $_POST['title']);
							$view->create();
							die();
						}
				    break;
				case self::DEL_OP_NAME :
				    if (!$this->model->deleteBook($_POST['id']))
						{
							$view = new ErrorView("Failed to delete book " . $_POST['id']);
							$view->create();
							die();
						}
				    break;
				case self::MOD_OP_NAME :
				    $book = new Book($_POST['title'], $_POST['author'], $_POST['description'], $_POST['id']);
				    if (!$this->model->modifyBook($book))
						{
							$view = new ErrorView("Failed to update book " . $_POST['id']);
							$view->create();
							die();
						}
				    break;
				}
			}

			// no special book is requested, we'll show a list of all available books
			$books = $this->model->getBookList();
			if ($books)
			{
				$view = new BookListView($books);
				$view->create();
			}
			else
			{
				$view = new ErrorView("Could not fetch the table");
				$view->create();
			}
		}
	}
}

?>
