<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;
    public $conn; // connected bool

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
	    else
	    {
			$dsn = 'mysql:host=localhost;dbname=test;charset=utf8mb4';
			$username = 'TestDB';
			$password = '7CY5Cd5I6YCqjW1T';
			try {
			  $this->db = new PDO($dsn, $username, $password);
			  $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $ex) {
			  error_log($ex->getMessage());
			}
			$this->conn = ($this->db != NULL);
		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
      try {
        $stmt = $this->db->query("SELECT *
          FROM book");
        $booklist = $stmt->fetchAll(PDO::FETCH_CLASS);
        return $booklist;
      } catch (PDOException $ex) {
        error_log($ex->getMessage());
        return null;
      }
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
      try {
        if ($this->invalidId($id)) return null;
        $query = $this->db->prepare("SELECT *
          FROM book
          WHERE id = :id");
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();
        $res = $query->fetch(PDO::FETCH_ASSOC);
        if ($query->rowCount() == 0) return null; // May not always work on other DBS
        return new Book($res['title'], $res['author'], $res['description'], $res['id']);
      } catch (PDOException $ex) {
        error_log($ex->getMessage());
        return null;
      }
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
     * @return True|False Returns true if the operation succeeded; false otherwise.
     * @throws PDOException
     */
    public function addBook($book)
    {
      try {
        if ($this->invalidInput($book->title, $book->author)) return false;
        $query = $this->db->prepare("INSERT INTO book (title, author, description)
          VALUES (:at, :aa, :ad)");
        $query->bindValue(':at', $book->title, PDO::PARAM_STR);
        $query->bindValue(':aa', $book->author, PDO::PARAM_STR);
        $desc = $book->description !== "" ? $book->description : NULL;
        $query->bindValue(':ad', $desc, PDO::PARAM_STR);
        $query->execute();
        $book->id = $this->db->lastInsertId();
        return true;
      } catch (PDOException $ex) {
        error_log($ex->getMessage());
        return false;
      }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @return True|False Returns true if the operation succeeded; false otherwise.
     * @throws PDOException
     */
    public function modifyBook($book)
    {
      try {
        if ($this->invalidId($book->id)) return false;
        if ($this->invalidInput($book->title, $book->author)) return false;
        $query = $this->db->prepare("UPDATE book
          SET title=:mt, author=:ma, description=:md
          WHERE id=:mi");
        $query->bindValue(':mt', $book->title, PDO::PARAM_STR);
        $query->bindValue(':ma', $book->author, PDO::PARAM_STR);
        $query->bindValue(':md', $book->description, PDO::PARAM_STR);
        $query->bindValue(':mi', $book->id, PDO::PARAM_INT);
        $query->execute();
        return true;
      } catch (PDOException $ex) {
        error_log($ex->getMessage());
        return false;
      }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @return True|False Returns true if the operation succeeded; false otherwise.
     */
    public function deleteBook($id)
    {
      try {
        if ($this->invalidId($id)) return false;
        $query = $this->db->prepare("DELETE FROM book
          WHERE id=:di");
        $query->bindValue(':di', $id, PDO::PARAM_INT);
        $query->execute();
        return true;
      } catch (PDOException $ex) {
        error_log($ex->getMessage());
        return false;
      }
    }
    /** Checks whether the id is a numeric integer or not.
     * @param $id integer The id of the book to be validated.
     * @return True|False Returns false if valid; true otherwise.
     */
    private function invalidId($id)
    {
      return !(is_numeric($id) && is_int(intval($id)));
    }
    /** Checks whether the title and author are non-empty strings or not.
     * @param $title string The title of the book to be validated.
     * @param $author string The author of the book to be validated.
     * @return True|False Returns false if valid; true otherwise.
     */
    private function invalidInput($title, $author)
    {
      return !(strlen($title) > 0 && strlen($author) > 0);
    }
}

?>
